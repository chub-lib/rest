package validator

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
	"gitlab.com/chub-lib/rest/constant"
)

var (
	v     *validator.Validate
	trans ut.Translator
)

func getValidator() (*validator.Validate, ut.Translator) {
	return v, trans
}

func init() {
	translator := en.New()
	uni := ut.New(translator, translator)

	// this is usually known or extracted from http 'Accept-Language' header
	var found bool
	trans, found = uni.GetTranslator("en")
	if !found {
		fmt.Println("translator not found")
	}

	v = validator.New()

	if err := en_translations.RegisterDefaultTranslations(v, trans); err != nil {
		fmt.Println(err)
	}

	_ = v.RegisterTranslation("required", trans, func(ut ut.Translator) error {
		return ut.Add("required", "{0} tidak boleh kosong", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T("required", fe.Field())
		return t
	})

	_ = v.RegisterTranslation("numeric", trans, func(ut ut.Translator) error {
		return ut.Add("numeric", "{0} harus berupa angka", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T("numeric", fe.Field())
		return t
	})

	v.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	_ = v.RegisterValidation("campaigndate", func(fl validator.FieldLevel) bool {
		val := fl.Field().String()
		_, err := time.Parse(constant.DATE_TIME_LAYOUT_NON_TZ, val)
		return err == nil
	})

	_ = v.RegisterTranslation("campaigndate", trans, func(ut ut.Translator) error {
		return ut.Add("campaigndate", "{0} must be in format 'yyyy-mm-dd hh:mm:ss'", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T("campaigndate", fe.Field())
		return t
	})

	_ = v.RegisterValidation("datedob", func(fl validator.FieldLevel) bool {
		val := fl.Field().String()
		_, err := time.Parse(constant.DATE_LAYOUT, val)
		return err == nil
	})

	_ = v.RegisterTranslation("datedob", trans, func(ut ut.Translator) error {
		return ut.Add("datedob", "{0} must be in format 'yyyy-mm-dd'", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T("datedob", fe.Field())
		return t
	})
}

func GetAndValidatePersonalNumber(c *gin.Context) (string, error) {
	personalNumber := c.GetHeader("X-CHUB-PERSONAL-NUMBER")
	if len(personalNumber) != 8 {
		return "", errors.New("invalid personal number")
	}
	if _, err := strconv.ParseInt(personalNumber, 10, 64); err != nil {
		return "", errors.New("personal number should be numeric")
	}
	return personalNumber, nil
}
