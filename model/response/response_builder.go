package response

import (
	"net/http"
	"strings"

	"gitlab.com/chub-lib/rest/constant"
)

func BuildSuccessResponse(data interface{}) *ResponseContainer {
	return &ResponseContainer{
		Response: Response{
			StatusCode:      http.StatusOK,
			ErrorCode:       &constant.ERROR_CODE_SUCCESS,
			ResponseCode:    &constant.RESPONSE_CODE_SUCCESS,
			ResponseMessage: &constant.RESPONSE_MESSAGE_SUCCESS,
			Errors:          nil,
			Data:            data,
			Info:            nil,
		},
	}
}

func BuildSuccessResponseWithInfo(data interface{}, info *ResponseInfo) *ResponseContainer {
	return &ResponseContainer{
		Response: Response{
			StatusCode:      http.StatusOK,
			ErrorCode:       &constant.ERROR_CODE_SUCCESS,
			ResponseCode:    &constant.RESPONSE_CODE_SUCCESS,
			ResponseMessage: &constant.RESPONSE_MESSAGE_SUCCESS,
			Errors:          nil,
			Data:            data,
			Info:            info,
		},
	}
}

func BuildDataNotFoundResponse() *ErrorContainer {
	return &ErrorContainer{
		Response: Response{
			StatusCode:      http.StatusNotFound,
			ErrorCode:       &constant.ERROR_CODE_DATA_NOT_FOUND,
			ResponseCode:    &constant.RESPONSE_CODE_NOT_FOUND,
			ResponseMessage: &constant.RESPONSE_MESSAGE_DATA_NOT_FOUND,
			Errors:          nil,
			Data:            nil,
			Info:            nil,
		},
	}
}

func BuildDataNotFoundResponseWithMessage(msg string) *ErrorContainer {
	return &ErrorContainer{
		Response: Response{
			StatusCode:      http.StatusNotFound,
			ErrorCode:       &constant.ERROR_CODE_DATA_NOT_FOUND,
			ResponseCode:    &constant.RESPONSE_CODE_NOT_FOUND,
			ResponseMessage: &constant.RESPONSE_MESSAGE_DATA_NOT_FOUND,
			Errors:          strings.Split(msg, "\n"),
			Data:            nil,
			Info:            nil,
		},
	}
}

func BuildBadRequestResponse(errCode, respCode, errMessage, throwable string) *ErrorContainer {
	return &ErrorContainer{
		Response: Response{
			StatusCode:      http.StatusBadRequest,
			ErrorCode:       &errCode,
			ResponseCode:    &respCode,
			ResponseMessage: &errMessage,
			Errors:          strings.Split(throwable, "\n"),
			Data:            nil,
			Info:            nil,
		},
	}
}

func BuildInternalErrorResponse(errCode, respCode, errMessage, throwable string) *ErrorContainer {
	return &ErrorContainer{
		Response: Response{
			StatusCode:      http.StatusInternalServerError,
			ErrorCode:       &errCode,
			ResponseCode:    &respCode,
			ResponseMessage: &errMessage,
			Errors:          strings.Split(throwable, "\n"),
			Data:            nil,
			Info:            nil,
		},
	}
}

func BuildRouteNotFoundResponse() *ErrorContainer {
	return &ErrorContainer{
		Response: Response{
			StatusCode:      http.StatusNotFound,
			ErrorCode:       &constant.ERROR_CODE_DATA_NOT_FOUND,
			ResponseCode:    &constant.RESPONSE_CODE_NOT_FOUND,
			ResponseMessage: &constant.RESPONSE_MESSAGE_ROUTE_NOT_FOUND,
			Errors:          nil,
			Data:            nil,
			Info:            nil,
		},
	}
}
